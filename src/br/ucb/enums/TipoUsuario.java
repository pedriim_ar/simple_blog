package br.ucb.enums;

/**
 * Enumerador que define o tipo de acesso que cada usu�rio possuir� numa p�gina.
 * As categorias de acesso devem respeitar a ordem, sendo 0 o maior n�vel de acesso poss�vel
 * e N o menor n�vel de acesso poss�vel.
 * 
 * @author pedro
 *
 */
public enum TipoUsuario {
	AMINISTRADOR,
	USUARIO
}
