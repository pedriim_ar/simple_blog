package br.ucb.managedBean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.NoResultException;

import br.ucb.bean.Usuario;
import br.ucb.bean.dao.UsuarioDAO;
import br.ucb.utils.FMessages;

@ManagedBean(name = "loginMB", eager = true)
@SessionScoped
public class LoginMB implements Serializable {
	private static final long serialVersionUID = 1L;

	private UsuarioDAO dao;
	private Usuario usuario;
	private String param_username;
	private String param_senha;
	
	public LoginMB() {
		this.dao = new UsuarioDAO();
		this.usuario = null;
	}
	
	public String login() {
		try {
			this.usuario = dao.searchByUsername(param_username);
		} catch (NoResultException e) {
			this.usuario = null;
		}
		
		if ((this.usuario == null) ||
				(this.usuario != null && !(this.usuario.getSenha().equals(param_senha)))) {
			this.usuario = null;
			FMessages.completeMessage("ERRO", "Usu�rio ou senha invalidos", FMessages.FATAL);
			return "";
		}			
		
		if (this.usuario != null) {
			System.out.println(this.usuario.toString());
			FMessages.completeMessage("SUCESSO", "Login sucedido", FMessages.INFO);
		}
		
		return "/index";
	}
	
	public String logoff() {
		this.usuario = null;
		return "/index";
	}
	
	public boolean isLogged() {
		return (this.usuario == null) ? false : true;
	}
	
	public boolean userIsTheSame(Usuario u) {
		return (this.usuario.getId() == u.getId()) ? true : false;
	}

	public UsuarioDAO getDao() {
		return dao;
	}

	public void setDao(UsuarioDAO dao) {
		this.dao = dao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getParam_username() {
		return param_username;
	}

	public void setParam_username(String param_username) {
		this.param_username = param_username;
	}

	public String getParam_senha() {
		return param_senha;
	}

	public void setParam_senha(String param_senha) {
		this.param_senha = param_senha;
	}
}
