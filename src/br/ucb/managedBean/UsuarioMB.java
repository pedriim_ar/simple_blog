package br.ucb.managedBean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.ucb.bean.Usuario;
import br.ucb.bean.dao.UsuarioDAO;
import br.ucb.utils.FMessages;
import br.ucb.enums.TipoUsuario;

@ManagedBean(name = "usuarioMB", eager = true)
@RequestScoped
public class UsuarioMB implements Serializable {
	private static final long serialVersionUID = 1L;

	private UsuarioDAO dao;
	private Usuario usuario;
	private String url_id;
	@SuppressWarnings("unused")
	private List<Usuario> todosUsuarios;
	
	public UsuarioMB() {
		this.dao = new UsuarioDAO();
	}
	
	public void novoUsuario() {
		this.usuario = new Usuario();
	}
	
	public String salvar() {
		if (this.usuario.getId() == 0) {
			if (!this.dao.insert(usuario)) {
				FMessages.completeMessage("Oooops", "Erro ao Salvar Usu�rio", FMessages.FATAL);
				return "";
			} else {
				FMessages.completeMessage("Sucesso", "Sucesso ao salvar usuario", FMessages.INFO);
				return "";
			}
		}
		
		FMessages.completeMessage("Ooops", "Algum erro ocorreu. Tente novamente mais tarde", FMessages.FATAL);
		return "";
	}
	
	public TipoUsuario[] getTipos() {
		return TipoUsuario.values();
	}
	
	public void buscarUsuario() {
		usuario = null;

		try {
			usuario = this.dao.findById(Long.parseLong(this.url_id));
		} catch (NumberFormatException e) {
			
		}
		
		if (usuario == null)
			FMessages.completeMessage("Oh n�o!",
					"Esse usu�rio n�o existe",
					FMessages.FATAL);
	}
	
	public void excluir(Usuario u) {
		try {
			if (dao.remove(u))
				FMessages.completeMessage("Sucesso",
						"Usuario " + u.getUsername() + " removido.",
						FMessages.INFO);
			else
				FMessages.completeMessage("Ooooops",
						"Usu�rio " + u.getNome() + " n�o foi removido.",
						FMessages.ERROR);
		} catch (Exception e) {
			FMessages.completeMessage("FATAL", e.getMessage(), FMessages.FATAL);
		}
		
		this.getTodosUsuarios();
	}
	
	public boolean isLogged() {
		return false;
	}

	public UsuarioDAO getDao() {
		return dao;
	}

	public void setDao(UsuarioDAO dao) {
		this.dao = dao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getUrl_id() {
		return url_id;
	}

	public void setUrl_id(String url_id) {
		this.url_id = url_id;
	}

	public List<Usuario> getTodosUsuarios() {
		return dao.list();
	}
}
