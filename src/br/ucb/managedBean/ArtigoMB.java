package br.ucb.managedBean;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.ucb.bean.Artigo;
import br.ucb.bean.Tag;
import br.ucb.bean.Usuario;
import br.ucb.bean.dao.ArtigoDAO;
import br.ucb.bean.dao.UsuarioDAO;
import br.ucb.utils.FMessages;

@ManagedBean(name = "artigoMB", eager = true)
@RequestScoped
public class ArtigoMB implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Artigo artigo;
	private ArtigoDAO dao;
	private UsuarioDAO udao;
	private String url_id;
	private List<Artigo> todosArtigos;
	
	public ArtigoMB () {
		this.dao = new ArtigoDAO();
		this.udao = new UsuarioDAO();
	}
	
	public void novoArtigo() {
		this.artigo = new Artigo();
	}
	
	public void adicionarTag(Long tag_id) {
		Tag t = new Tag();
		t.setId(tag_id);
		
		this.artigo.getTags().add(t);
	}
	
	public String salvarArtigo(String username_autor) {
		if (this.artigo.getId() == 0) {
			Usuario a = udao.searchByUsername(username_autor);
			
			this.artigo.setPublicacao(LocalDateTime.now());
			this.artigo.setAutor(a);
			
			if (!this.dao.insert(artigo)) {
				FMessages.completeMessage("Oooops", "Erro ao salvar o artigo", FMessages.FATAL);
				return "";
			} else {
				FMessages.completeMessage("Sucesso", "Artigo salvo e publicado com sucesso", FMessages.INFO);
				return "/paginas/usuarios/usuario.jsf?id=" + a.getId();
			}	
		} else {
			FMessages.completeMessage("Oooops", "Algum erro ocorreu. Tente novamente mais tarde", FMessages.FATAL);
			return "";
		}
	}
	
	public void buscarArtigo() {
		artigo = null;
		
		try {
			artigo = this.dao.findById(Long.parseLong(this.url_id));
		} catch (NumberFormatException e) {
			
		}
		
		if (artigo == null)
			FMessages.completeMessage("Oh n�o!",
					"ARTIGO INEXISTENTE",
					FMessages.FATAL);
	}
	
	public void recuperarArtigos() {
		todosArtigos = null;
		todosArtigos = dao.list();
		
		if (todosArtigos == null || todosArtigos.size() == 0)
			FMessages.completeMessage("Oh n�o!",
					"N�o existem artigos publicados",
					FMessages.FATAL);
	}

	public Artigo getArtigo() {
		return artigo;
	}

	public void setArtigo(Artigo artigo) {
		this.artigo = artigo;
	}

	public String getUrl_id() {
		return url_id;
	}

	public void setUrl_id(String url_id) {
		this.url_id = url_id;
	}

	public List<Artigo> getTodosArtigos() {
		return todosArtigos;
	}

	public void setTodosArtigos(List<Artigo> todosArtigos) {
		this.todosArtigos = todosArtigos;
	}
}
