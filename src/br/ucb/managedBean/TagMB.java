package br.ucb.managedBean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.ucb.bean.Tag;
import br.ucb.bean.dao.TagDAO;
import br.ucb.utils.FMessages;

@ManagedBean(name = "tagMB", eager = true)
@RequestScoped
public class TagMB implements Serializable {
	private static final long serialVersionUID = 1L;

	private TagDAO dao;
	private Tag tag;
	private String url_id;
	
	public TagMB() {
		this.dao = new TagDAO();
	}
	
	public void buscarTag() {
		tag = null;
		
		try {
			tag = this.dao.findById(Long.parseLong(this.url_id));
		} catch (NumberFormatException e) {
			
		}
		
		if (tag == null)
			FMessages.completeMessage("Oh n�o!",
					"TAG INEXISTENTE",
					FMessages.FATAL);
	}

	public TagDAO getDao() {
		return dao;
	}

	public void setDao(TagDAO dao) {
		this.dao = dao;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public String getUrl_id() {
		return url_id;
	}

	public void setUrl_id(String url_id) {
		this.url_id = url_id;
	}
}
