package br.ucb.utils;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public abstract class FMessages {
	public static final Severity INFO = FacesMessage.SEVERITY_INFO;
	public static final Severity ERROR = FacesMessage.SEVERITY_ERROR;
	public static final Severity WARN = FacesMessage.SEVERITY_WARN;
	public static final Severity FATAL = FacesMessage.SEVERITY_FATAL;
	
	public static void createMessage(String message_summary,
			String message_detail, Severity severity) {
		FacesContext context = FacesContext.getCurrentInstance();
		
		FacesMessage f_message = new FacesMessage(severity, message_summary,
				message_detail);
		
		context.addMessage(null, f_message);
	}
	
	public static void simpleMessage(String message, Severity sev) {
		FMessages.createMessage(message, message, sev);
	}
	
	public static void completeMessage(String summary, String detail, Severity severity) {
		FMessages.createMessage(summary, detail, severity);
	}
}
