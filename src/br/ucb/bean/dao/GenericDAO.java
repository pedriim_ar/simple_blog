package br.ucb.bean.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@SuppressWarnings({"unchecked", "rawtypes"})
public abstract class GenericDAO<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected EntityManager em;
	
	private Class genericClass;
	
	public GenericDAO () {
		this.genericClass = (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.em = ConnectionFactory.getEntityManager();
	}

	public boolean insert(T object) {
		em.getTransaction().begin();
		em.persist(object);
		
		try {
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public T findById(long id) {
		return (T) em.find(this.genericClass, id);
	}
	
	public boolean remove(T object) {
		em.getTransaction().begin();
		em.remove(object);
		
		try {
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean update(T object) {
		em.getTransaction().begin();
		em.merge(object);
		
		try {
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void refresh(T object) {
		em.refresh(object);
	}
	
	public List<T> list() {
		Query list = em.createQuery("SELECT t FROM " + this.genericClass.getName() + " t");
		
		return list.getResultList();
	}
}
