package br.ucb.bean.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class ConnectionFactory implements Serializable {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory emf = null;
	private static EntityManager em = null;
	
	public static EntityManager getEntityManager() {
		try {
			if (emf == null)
				emf = Persistence.createEntityManagerFactory("blog");
		} catch (Exception e){
			System.out.println("Erro na cria��o do EntityManagerFactory "+ e);
		}
		
		try {
			if (em == null)
				em = emf.createEntityManager();
		} catch (Exception e){
			System.out.println("Erro na cria��o do EntityManager "+ e);
		}
		
		return em;
	}
}
