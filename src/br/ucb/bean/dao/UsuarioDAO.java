package br.ucb.bean.dao;

import javax.persistence.Query;

import br.ucb.bean.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario> {
	private static final long serialVersionUID = 1L;

	public Usuario searchByUsername(String username) {
		Query result = super.em.createQuery("SELECT u FROM Usuario u WHERE u.username = '" + username + "'");
		
		return (Usuario) result.getSingleResult();
	}
}
