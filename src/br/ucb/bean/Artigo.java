package br.ucb.bean;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Artigo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String titulo;
	private String texto;
	private LocalDateTime publicacao;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario autor;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "artigo_possui_tag",
			joinColumns = {
					@JoinColumn(name = "id_artigo")
			},
			inverseJoinColumns = {
					@JoinColumn(name = "id_tag")
			}
	)
	private List<Tag> tags;

	public Artigo() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public LocalDateTime getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(LocalDateTime publicacao) {
		this.publicacao = publicacao;
	}
	
	public void setPublicacaoAgora() {
		this.publicacao = LocalDateTime.now();
	}

	public Usuario getAutor() {
		return autor;
	}

	public void setAutor(Usuario publicadoPor) {
		this.autor = publicadoPor;
	}
	
	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public String getDataPublicacao() {
		return this.publicacao.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
	}
	
	public String getHoraPublicacao() {
		return this.publicacao.format(DateTimeFormatter.ofPattern("HH:mm"));
	}
	
	@Override
	public String toString() {
		return String.format("[Artigo %d]\nTitulo: %s\nData: %s\nTexto: %s\nPublicado por: %s",
				this.id, this.titulo, this.publicacao.format(DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm"))
				, this.texto, this.autor.getNome());
	}
}
