CREATE DATABASE TALP_PROJETO;

USE TALP_PROJETO;

CREATE TABLE usuario (
    id INT AUTO_INCREMENT,
    nome VARCHAR(256) NOT NULL,
    bio VARCHAR(512) NOT NULL,
    tipo TINYINT(1) NOT NULL,
    senha VARCHAR(45) NOT NULL,
    username VARCHAR(45) NOT NULL,
    
    CONSTRAINT pk_usuario
    PRIMARY KEY (id)
);

CREATE TABLE artigo (
	id INT AUTO_INCREMENT,
    titulo VARCHAR(256) NOT NULL,
    texto TEXT NOT NULL,
    publicacao DATETIME NOT NULL,
    
    id_usuario INT NOT NULL,
    
    CONSTRAINT pk_artigo
    PRIMARY KEY (id),
    
    CONSTRAINT fk_id_usuario
    FOREIGN KEY (id_usuario)
    REFERENCES usuario(id)    
);

CREATE TABLE tag (
	id INT AUTO_INCREMENT,
    nome VARCHAR(32) NOT NULL,
    
    CONSTRAINT pk_tag
    PRIMARY KEY (id)
);

CREATE TABLE artigo_possui_tag (
	id_artigo INT NOT NULL,
    id_tag INT NOT NULL,
    
    CONSTRAINT pk_artigo_possui_tag
    PRIMARY KEY (id_artigo, id_tag),
    
    CONSTRAINT fk_id_artigo
    FOREIGN KEY (id_artigo)
    REFERENCES artigo(id),
    
    CONSTRAINT fk_id_tag
    FOREIGN KEY (id_tag)
    REFERENCES tag(id)
);